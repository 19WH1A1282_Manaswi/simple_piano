import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { KeyboardComponent } from './keyboard/keyboard/keyboard.component';
import { VolumnSilderComponent } from './volumn-silder/volumn-silder.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
   { path: 'keyboard', component: KeyboardComponent },
   
  { path: 'volumn-silder', component: VolumnSilderComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }