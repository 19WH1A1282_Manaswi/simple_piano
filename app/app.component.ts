import { Component } from '@angular/core';
import { ShowdetailsComponent } from './showdetails/showdetails.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLogin:boolean=true;
  isRegister:boolean=false
  title = 'simple';
  public titles: string = 'Simple Piano';
  ngOnInit() {
    Howler.volume(1.0);
  }
  
  constructor(){
    
    // setTimeout(() => {
    //   this.isRegister=true;
    //   this.isLogin=false
    // }, 6000);
  }
}


  
