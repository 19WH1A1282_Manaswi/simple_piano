import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KeyboardComponent } from './keyboard/keyboard/keyboard.component';
import { VolumnSilderComponent } from './volumn-silder/volumn-silder.component';
import { KeyDirective } from './key.directive';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { ShowdetailsComponent } from './showdetails/showdetails.component';
import { ExpPipe } from './exp.pipe';
import { RouterModule } from '@angular/router';
import 'howler';

@NgModule({
  declarations: [
    AppComponent,
    KeyboardComponent,
    VolumnSilderComponent,
    KeyDirective,
    LoginComponent,
    RegisterComponent,
    ShowdetailsComponent,
    ExpPipe

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { } 
