import { Directive, HostListener, ElementRef, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Howl } from 'howler';

@Directive({
  selector: '[key]'
})
export class KeyDirective implements OnInit, OnChanges {
  //  sound: typeof Howler | ;
  
  @Input() playing: boolean = false;
  keysound: number | undefined;
  constructor() {

  }
  @Input() volume: number = 1.0;
  @Input() keyboardCode: number | undefined;
  @Input() soundSource: string[] | undefined;
  @Input() rate: number = 1.0;
  @Output() pressed = new EventEmitter();
  ngOnInit() {
      this.sound = new Howl({
       src: this.soundSource,
       volume: this.volume,
        rate: this.rate
      });
  }
  
  sound = new Howl({
    src: ['https://www.kozco.com/tech/piano2-CoolEdit.mp3'],
    html5 :true,
    onend: function() {
      console.log('stop')
     }
  });

   keyplay()  {
    this.sound.play();
   }

    keystop():void {
     this.sound.unload()

   }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty("playing") && !changes['playing'].isFirstChange()) {
      changes['playing'].currentValue ? this.play() : this.stop();
    }
  }
  @HostListener('mousedown') onMouseDown() {
    this.play();
  }
  @HostListener('mouseup') onMouseUp() {
    this.stop();
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.stop();
  }
  @HostListener('mouseover', ['$event']) onMouseOver(event: { buttons: number; }) {
    if (event.buttons === 1)
      this.play();
  }
  @HostListener('dragstart') onDrag() {
    return false;
  }
  @HostListener('document:keydown', ['$event']) onKeyDown(event: { keyCode: number | undefined; }) {
    if (event.keyCode == this.keyboardCode && !this.playing) {
      this.play();
    }
  }
  @HostListener('document:keyup', ['$event']) onKeyUp(event: { keyCode: number | undefined; }) {
    if (event.keyCode == this.keyboardCode) {
      this.stop();
    }
  }
  private play() {
     if (this.sound.playing())
    this.sound.stop();
     this.playing = true;
     this.sound.play();
    this.pressed.emit(this.playing);
  }
  stop() {
    this.sound.fade(1.0, 0, 200);
    this.sound.stop();
     this.playing = false;
     this.pressed.emit(this.playing);
  }

}
