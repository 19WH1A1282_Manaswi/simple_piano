import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:any;

  constructor(private service:UsersService,private router:Router) { 
    this.user={name:' ',sname:' ',email:' ',password:' '};
  }

  ngOnInit(): void {
  }
  
  checkPwd(){
    console.log("Check");

  }
  async signUp(registerForm:any){
    // alert(this.email);
    console.log(registerForm.password);
    console.log(registerForm.c_password);
    this.user.username = registerForm.username;
    this.user.email = registerForm.email;
    this.user.password = registerForm.password;
  
    console.log(this.user.username);
    console.log(this.user.email);
    console.log(this.user.password);


    if(registerForm.password===registerForm.c_password){
      // this.service.signUp(this.userdetails).subscribe();
      this.userRegister();
      // alert("Registered");

    }
    else{
      alert("Check your password");
    }
  }
  // userRegister(){
  //   this.service.userRegister(this.userdetails).subscribe();
  // }
  userRegister(){
    this.service.userRegister(this.user).subscribe();
    this.service.showAllUsers().subscribe();
    // alert("Registered");

  }

}
