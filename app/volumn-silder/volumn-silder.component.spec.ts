import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VolumnSilderComponent } from './volumn-silder.component';

describe('VolumnSilderComponent', () => {
  let component: VolumnSilderComponent;
  let fixture: ComponentFixture<VolumnSilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VolumnSilderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VolumnSilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
