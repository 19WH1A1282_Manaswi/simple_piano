import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-volumn-silder',
  templateUrl: './volumn-silder.component.html',
  styleUrls: ['./volumn-silder.component.css']
})
export class VolumnSilderComponent implements OnInit {
  min: number = 0;
  max: number = 1;
  isMouseDown: boolean = false;
  @Input() value: number = 0.7;
  initialMousePosition: number | undefined;
  mouseWheelScaling: number = 50;
  mouseMoveScaling: number = 2000;
  rotationStyle: string = this.sanitizeRotation(this.value);
  @Output() levelChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.levelChange.emit(this.value);
  }

  onMouseUp(e:any) {
    this.isMouseDown = false;
  }
  onMouseDown(e: any) {
    this.isMouseDown = true;
  }
  onMouseMove(e: { clientX: number | undefined; }) {
    if (this.isMouseDown) {
      if (this.initialMousePosition) {
        let diff = this.newMethod(e) - this.initialMousePosition;
        this._setNewValue({ newValue: this.value + diff / this.mouseMoveScaling });
      }
      else {
        this.initialMousePosition = e.clientX;
      }
    }

  }
    newMethod(e:any) {
        return e.clientX;
    }

  _setNewValue({ newValue }: { newValue: any; }): void {
    if (newValue < this.min) {
      this.value = this.min;
    } else if (newValue > this.max) {
      this.value = this.max;
    } else {
      this.value = newValue;
    }
    this.rotationStyle = this.sanitizeRotation(this.value);
    this.levelChange.emit(this.value);
  }
  onWheelDragging(e: any): void {
    if (e.preventDefault)
      e.preventDefault();
    let direction = (e.deltaY < 0) ? 1 : -1;
    let newValue: number = Number.parseFloat((this.value + direction / this.mouseWheelScaling).toFixed(2));
    this._setNewValue({ newValue });
  }

  sanitizeRotation(val: number) {
    return `rotate(${Math.round(val * 270) - 90}deg)`;
  }

}
